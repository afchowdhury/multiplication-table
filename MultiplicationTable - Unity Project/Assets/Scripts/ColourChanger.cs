﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColourChanger : MonoBehaviour
{
    public Color yellow, white;
    public bool isSelected;

    public NumberGenerator numberGenerator;
    
    // Start is called before the first frame update
    void Start()
    {
        yellow = new Color(255,0,255);
        white = new Color(255,255,255);

        isSelected = false;
    }

    public void SelectNumber()
    {
        if(isSelected)
        {
            isSelected = !isSelected;
            gameObject.GetComponent<Image>().color = white;
        }
        else
        {
            isSelected = !isSelected;
            gameObject.GetComponent<Image>().color = yellow;
        }

        numberGenerator.RightRange();
    }

}
