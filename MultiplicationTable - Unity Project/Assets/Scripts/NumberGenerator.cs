﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberGenerator : MonoBehaviour
{
    //pulling in timestables script
    public TimesTables timesTables;
    
    //List of game objects for buttons
    public List<GameObject> buttonsList, multiSymbol = new List<GameObject>();

    //left list which has list of integers from 1 to 12
    public List<int> leftList = new List<int>();

    //right list which has a list of integers from 2 to 12
    public List<int> rightList = new List<int>();

    public Color yellow, white;

    public int currNumber;

    public bool allOn;

    // Start is called before the first frame update
    void Start()
    {
        //have a for loop that sets the left list to be 1-12
        for(int i=1; i<13; i++)
        {
           leftList.Add(i);
        }
        //foreach loop that sets left buttons to be 1-12 in order
        currNumber = 0;
        
        RightRange();
        foreach(GameObject x in multiSymbol)
        {
            x.GetComponent<TimesTables>().leftNumber = leftList[currNumber];
            currNumber++;
        }

        allOn = false;
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }

    public void RightRange()
    {
        rightList = new List<int>();

        //if the button is changed to yellow, then take what int it is and add it to the list
        foreach(GameObject x in buttonsList)
        {
            if(x.GetComponent<ColourChanger>().isSelected)
            {
                rightList.Add(int.Parse(x.transform.GetChild(0).gameObject.GetComponent<Text>().text));
            }
        }

        if(rightList.Count < 1)
        {
            rightList.Add(0);
        }
        
        foreach(GameObject x in multiSymbol)
        {
            x.GetComponent<TimesTables>().ResetDisplay();
            x.GetComponent<TimesTables>().RandomNumber();
        }
    }

    //Allbutton changes all the buttons to yellow and adds everything to the list. Then changes all the buttons to white when pressed again
    public void AllButton()
    {
        if(allOn)
        {
            foreach(GameObject x in buttonsList)
            {
                x.GetComponent<Image>().color = x.GetComponent<ColourChanger>().white;
                x.GetComponent<ColourChanger>().isSelected = false;
                
            }
            allOn = !allOn;
        }
        else
        {
            foreach(GameObject x in buttonsList)
            {
                x.GetComponent<Image>().color = x.GetComponent<ColourChanger>().yellow;
                x.GetComponent<ColourChanger>().isSelected = true;
                
            }
            allOn = !allOn;
        }

        RightRange();
        
    }

    //Reset button uses reset display function from the times tables script
    public void ResetButton()

    {
        foreach(GameObject x in multiSymbol)
        {
            x.GetComponent<TimesTables>().ResetDisplay();
            x.GetComponent<TimesTables>().RandomNumber();
        }
    }
}
