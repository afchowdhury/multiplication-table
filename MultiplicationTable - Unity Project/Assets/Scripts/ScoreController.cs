﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreController : MonoBehaviour
{
    public GameObject totalLeft, totalRight, toggleCountersText, toggleTotalsText, totalLeftButton, totalRightButton;
    public List<GameObject> counterList = new List<GameObject>();
    private Text totalLeftText, totalRightText;
    private string countTotalText;
    private int countTotal, sumOf;
    private GameObject currCounter;
    private bool isLeftCovered, isRightCovered, showTotals, showCounter;
    
    // Start is called before the first frame update
    void Start()
    { 
        totalLeftText = totalLeft.GetComponent<Text>();
        totalRightText = totalRight.GetComponent<Text>();

        showCounter = false;
        showTotals = false;

        ToggleCounters();
        ToggleTotals();

        RefreshTotal();
    }

    public void RefreshTotal()
    {
        totalRightText.text = "??";
        totalLeftText.text = "??";
        isLeftCovered = true;
        isRightCovered = true;
    }

    public void CalculateTotal(int calcSide)
    {
        countTotal = 0;
        
        if(calcSide == 1)
        {
            
            if(isLeftCovered)
            {
                for(int i=0; i<2; i++)
                {
                    currCounter = counterList[i];
                    countTotal += int.Parse(currCounter.GetComponent<Text>().text);
                }
            
                totalLeftText.text = countTotal.ToString();
                isLeftCovered = !isLeftCovered;
            }
            else
            {
                countTotal = 0;
                totalLeftText.text = totalLeftText.text = "??";
                isLeftCovered = !isLeftCovered;
            }

        }
        else
        {
            if(isRightCovered)
            {
                for(int i=2; i<4; i++)
                {
                    currCounter = counterList[i];
                    countTotal += int.Parse(currCounter.GetComponent<Text>().text);
                }
            
                totalRightText.text = countTotal.ToString();
                isRightCovered = !isRightCovered;
            }
            else
            {
                countTotal = 0;
                totalRightText.text = totalRightText.text = "??";
                isRightCovered = !isRightCovered;
            }
        }
    }

    public void ToggleCounters()
    {
        if(showCounter)
        {
            for(int i=0; i<4; i++)
            {
                currCounter = counterList[i];
                currCounter.SetActive(false);
            }
            showCounter = !showCounter;
            toggleCountersText.GetComponent<Text>().text = "Show Counters";           
        }
        else
        {
            for(int i=0; i<4; i++)
            {
                currCounter = counterList[i];
                currCounter.SetActive(true);
            }
            showCounter = !showCounter;
            toggleCountersText.GetComponent<Text>().text = "Hide Counters";
        }
    }

    public void ToggleTotals()
    {
        if(showTotals)
        {
            totalLeftButton.SetActive(false);
            totalRightButton.SetActive(false);

            showTotals = !showTotals;
            toggleTotalsText.GetComponent<Text>().text = "Show Totals";
        }
        else
        {
            totalLeftButton.SetActive(true);
            totalRightButton.SetActive(true);
            
            showTotals = !showTotals;
            toggleTotalsText.GetComponent<Text>().text = "Hide Totals";
        }
    }
    
}
