﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoveBead : MonoBehaviour
{
    public GameObject displayLeftCount, displayRightCount, scoreController;
    private GameObject currentBead;
    private RectTransform currentBeadRect;
    public List<GameObject> beadsList = new List<GameObject>();
    public int currentBeadNumber;
    public Vector2 maxAnc, minAnc;
    public bool allRight, allLeft;
    private int leftCount, rightCount;
    private ScoreController scoreControl;

    // Start is called before the first frame update
    void Start()
    {
        scoreControl = scoreController.GetComponent<ScoreController>();
        currentBeadNumber = 0;
        leftCount = 0;
        rightCount = 10;

        foreach(GameObject bead in beadsList)
        {
            bead.GetComponent<RectTransform>().anchorMax = new Vector2( bead.GetComponent<RectTransform>().anchorMax.x + 0.55f, 0.5f);
            bead.GetComponent<RectTransform>().anchorMin = new Vector2( bead.GetComponent<RectTransform>().anchorMin.x + 0.55f, 0.5f);
        }
        
        RefreshCount();
        allLeft = false;
        allRight = true; 
    }

    public void BeadMove(int moveDir)
    {   
        scoreControl.RefreshTotal();
        
        if(moveDir > 0)
        {
            if(currentBeadNumber > 0)
            {
                currentBead = beadsList[currentBeadNumber];
                currentBeadRect = currentBead.GetComponent<RectTransform>();

                maxAnc = new Vector2((currentBeadRect.anchorMax.x + 0.55f),0.5f);
                minAnc = new Vector2((currentBeadRect.anchorMin.x + 0.55f),0.5f);

                currentBeadRect.anchorMax = maxAnc;
                currentBeadRect.anchorMin = minAnc;

                currentBeadNumber--;
                EditCount(moveDir);
                allLeft = false;
            }
            else if(currentBeadNumber == 0 && allRight == false)
            {
                currentBead = beadsList[currentBeadNumber];
                currentBeadRect = currentBead.GetComponent<RectTransform>();

                maxAnc = new Vector2((currentBeadRect.anchorMax.x + 0.55f),0.5f);
                minAnc = new Vector2((currentBeadRect.anchorMin.x + 0.55f),0.5f);

                currentBeadRect.anchorMax = maxAnc;
                currentBeadRect.anchorMin = minAnc;
                allRight = true;
                EditCount(moveDir);
            }

        }
        else
        {
            if(currentBeadNumber == 0 && allRight == true)
            {
                currentBead = beadsList[currentBeadNumber];
                currentBeadRect = currentBead.GetComponent<RectTransform>();

                maxAnc = new Vector2((currentBeadRect.anchorMax.x - 0.55f),0.5f);
                minAnc = new Vector2((currentBeadRect.anchorMin.x - 0.55f),0.5f);

                currentBeadRect.anchorMax = maxAnc;
                currentBeadRect.anchorMin = minAnc;
                EditCount(moveDir);
                allRight = false;
                
            }
            else if(currentBeadNumber < 8)
            {
                currentBeadNumber++;
                
                currentBead = beadsList[currentBeadNumber];
                currentBeadRect = currentBead.GetComponent<RectTransform>();

                maxAnc = new Vector2((currentBeadRect.anchorMax.x - 0.55f),0.5f);
                minAnc = new Vector2((currentBeadRect.anchorMin.x - 0.55f),0.5f);

                currentBeadRect.anchorMax = maxAnc;
                currentBeadRect.anchorMin = minAnc;
                EditCount(moveDir);
                
            }
            else if(currentBeadNumber == 8  && allLeft == false)
            {
                currentBeadNumber++;
                currentBead = beadsList[currentBeadNumber];
                currentBeadRect = currentBead.GetComponent<RectTransform>();

                maxAnc = new Vector2((currentBeadRect.anchorMax.x - 0.55f),0.5f);
                minAnc = new Vector2((currentBeadRect.anchorMin.x - 0.55f),0.5f);

                currentBeadRect.anchorMax = maxAnc;
                currentBeadRect.anchorMin = minAnc;
                allLeft = true;
                EditCount(moveDir);

            }
        }
        RefreshCount();
    }

    private void RefreshCount()
    {
        displayLeftCount.GetComponent<Text>().text = leftCount.ToString();
        displayRightCount.GetComponent<Text>().text = rightCount.ToString();
    }

    private void EditCount(int moveDir)
    {
        switch(moveDir)
        {
            case 1:
                leftCount --;
                rightCount ++;
            break;

            case -1:
                leftCount ++;
                rightCount --;
            break;
        }
    }

}
