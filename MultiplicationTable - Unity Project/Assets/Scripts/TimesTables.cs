﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimesTables : MonoBehaviour
{
    
    public int leftNumber, rightNumber, answerNumber, tempNumber;
    public bool leftDisplay, rightDisplay, answerDisplay;
    public Text leftText, rightText, answerText;
    public NumberGenerator numberGenerator;

    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void RandomNumber()
    {
        tempNumber = Random.Range(0,(numberGenerator.rightList.Count ));
        rightNumber = numberGenerator.rightList[tempNumber];

        answerNumber = leftNumber * rightNumber;
    }

    public void ResetDisplay()
    {
        leftDisplay = false;
        rightDisplay = false;
        answerDisplay = false;

        leftText.text = "";
        rightText.text = "";
        answerText.text = "";
    }   

    public void NumberDisplay(int buttonPressed)
    {        
        switch(buttonPressed)
        {
            case 1:               
                if(!leftDisplay)
                {
                    leftText.text = leftNumber.ToString();

                    leftDisplay = !leftDisplay;
                }
                else
                {
                    leftText.text = "";
                    leftDisplay = !leftDisplay;
                }
            break;

            case 2:
                if(!rightDisplay)
                {
                    rightText.text = rightNumber.ToString();

                    rightDisplay = !rightDisplay;
                }
                else
                {
                    rightText.text = "";
                    rightDisplay = !rightDisplay;
                }

            break;

            case 3:
                if(!answerDisplay)
                {
                    answerText.text = answerNumber.ToString();

                    answerDisplay = !answerDisplay;
                }
                else
                {
                    answerText.text = "";
                    answerDisplay = !answerDisplay;
                }

            break;
        }
    }


}
