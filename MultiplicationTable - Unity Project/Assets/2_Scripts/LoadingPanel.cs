﻿using System;
using System.Collections;
using TMPro;
using TwinklAuthenticationGateway;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class LoadingPanel : iTransitionPanel
{
    [SerializeField] private Animator animator = null;
    [SerializeField] private Image loadingBar = null;
    [SerializeField] private float loadSpeed = 0.0f;
    [SerializeField] private Image panel = null;
    [SerializeField] private TMP_Text LoadingText;
    [SerializeField] private bool DisplayWithoutAnimation;

    public override void Start()
    {
        base.Start();
            
    }

    public override void CoverScreen(Action _onComplete = null)
    {
        base.UncoverScreen();
        this.gameObject.SetActive(true);
        LoadingText.gameObject.SetActive(true);
        animator.SetTrigger("DropIn");
        loadingBar.fillAmount = 0;
        _onComplete?.Invoke();
    }

    public override void UncoverScreen(Action _onComplete = null)
    {        
        base.CoverScreen();
        
        if(DisplayWithoutAnimation || onStartAction != ActionOnStart.Hide)
            CoverScreen(() =>
            {
                StartCoroutine(iHide(_onComplete));
            });
        else
        {
            StartCoroutine(iHide(_onComplete));
        }
    }

    private IEnumerator iHide(Action onComplete)
    {
        StartCoroutine(FillLoadingBar());
        yield return new WaitForSeconds(3f);
        onComplete?.Invoke();
        this.gameObject.SetActive(false);
    }

    private IEnumerator FillLoadingBar()
    {
        float _elapsedTime = 0f;
        while (loadingBar.fillAmount < 1)
        {
            _elapsedTime += Time.deltaTime;
            loadingBar.fillAmount = 1f * (_elapsedTime/ defaultAnimationTime);
            yield return null;
        }
        animator.SetTrigger("LoadFinished");
    }
}