﻿
using TwinklStandardClasses;
using UnityEngine;

namespace TwinklGameManager
{
    /// <summary>
    /// Controls transition between Unity scenes.
    /// Various options for scene loading.
    /// </summary>
    public class AudioController : Singleton<AudioController>
    {
        private bool _muted;

        public bool muted
        {
            get { return _muted; }
            set
            {
                if (_muted != value)
                {
                    PlayerPrefs.SetInt("Muted", value ? 1 : 0);
                    _muted = value;
                    onMuteStateChanged?.Invoke(value);
                    updateListenerVolume();
                }
            }
        }

        public BoolEvent onMuteStateChanged = new BoolEvent();

        protected override void Awake()
        {
            _muted = PlayerPrefs.GetInt("Muted") == 0;
            updateListenerVolume();
        }

        private void updateListenerVolume()
        {
            AudioListener.volume = muted ? 0 : 1;
        }

    }
}