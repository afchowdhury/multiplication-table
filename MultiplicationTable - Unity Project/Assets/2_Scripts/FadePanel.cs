﻿using System;
using System.Collections;
using TwinklAuthenticationGateway;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;


[RequireComponent(typeof(Image))]
public class FadePanel : iTransitionPanel
{ 
    private Image panel => GetComponent<Image>();
   
    public override void Start()
    {
        base.Start();
    }

    public override void UncoverScreen(Action _onComplete = null)
    {
        base.UncoverScreen();
        StartCoroutine(FadeIn( _onComplete));
    }

    public override void CoverScreen(Action _onComplete = null)
    {
        base.CoverScreen();
        StartCoroutine(FadeOut(_onComplete));
    }
    
    private IEnumerator FadeOut(Action _onComplete = null, float _loadTime = -1f)
    { 
        _loadTime = _loadTime < 0 ? defaultAnimationTime : _loadTime;
        
        panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 1f);
        float _elapsedTime = 0f;
        while (panel.color.a > 0f)
        {
            _elapsedTime += Time.deltaTime;
            float _a = 1f * (_loadTime - _elapsedTime/ _loadTime);
            panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, _a);
            yield return null;
        }
        _onComplete?.Invoke();
    }
    
    private IEnumerator FadeIn(Action _onComplete = null, float _loadTime = -1f)
    {
        _loadTime = _loadTime < 0 ? defaultAnimationTime : _loadTime;
        float _elapsedTime = 0f;
        panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 0f);
        while (panel.color.a < 1f)
        {
            _elapsedTime += Time.deltaTime;
            float _a = 1f * (_elapsedTime/ _loadTime);
            panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, _a);
            yield return null;
        }
        _onComplete?.Invoke();
    }
}
